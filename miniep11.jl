using Unicode
function clean_string(s)
    s = Unicode.normalize(s, stripmark=true)
    n = length(s)
    ans = ""
    for i = 1:n
        if 'a' <= s[i] <= 'z'
            ans *= s[i]-32
        elseif 'A' <= s[i] <= 'Z'
            ans *= s[i]
        end
    end
    return ans
end

function palindromo(s)
    s = clean_string(s)
    n = length(s)
    for i = 1:div(n, 2)
        if s[i] != s[n-i+1]
            return false
        end
    end
    return true
end
